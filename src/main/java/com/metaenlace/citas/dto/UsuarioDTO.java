package com.metaenlace.citas.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UsuarioDTO {

    private Long id;
    private String nombre;
    private String apellidos;
    private String usuario;
    private String clave;
    private List<Long> pacientesId;
}
