package com.metaenlace.citas.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MedicoDTO {
    private Long id;
    private String numColegiado;
    private String usuario;
    private String clave;
    private List<Long> citasId;
    private List<Long> pacientesId;
}
