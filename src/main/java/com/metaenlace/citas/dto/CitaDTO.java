package com.metaenlace.citas.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
public class CitaDTO {

    private Long id;
    private LocalDateTime fechaHora;
    private String motivoCita;
    private Long diagnosticoId;
    private Long pacienteId;
    private Long medicoId;
}
