package com.metaenlace.citas.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PacienteDTO {

    private Long id;
    private String nss;
    private String numTarjeta;
    private String telefono;
    private String direccion;
    private String usuario;
    private List<Long> citasId;
    private List<Long> medicosId;
}
