package com.metaenlace.citas.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
public class DiagnosticoDTO {

    private Long id;
    private String valoracionEspecialista;
    private String enfermedad;
    private Long citaId;
}