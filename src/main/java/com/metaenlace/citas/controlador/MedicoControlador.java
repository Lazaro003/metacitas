package com.metaenlace.citas.controlador;

import com.metaenlace.citas.entidad.Medico;
import com.metaenlace.citas.servicio.MedicoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/medicos")
public class MedicoControlador {

    @Autowired
    private MedicoServicio medicoServicio;

    @GetMapping("/{id}")
    public Optional<Medico> getMedico(@PathVariable("id") Long id) {
        return medicoServicio.getMedicoById(id);
    }

    @PostMapping
    public Medico crearMedico(@RequestBody Medico medico) {
        return medicoServicio.crearMedico(medico);
    }

    @PutMapping("/{id}")
    public Medico guardarMedico(@PathVariable("id") Long id, @RequestBody Medico medico) {
        medico.setId(id);
        return medicoServicio.guardarMedico(medico);
    }

    @DeleteMapping("/{id}")
    public void eliminarMedico(@PathVariable("id") Long id) {
        medicoServicio.borrarMedico(id);
    }

}
