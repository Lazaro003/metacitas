package com.metaenlace.citas.controlador;

import com.metaenlace.citas.entidad.Diagnostico;
import com.metaenlace.citas.servicio.DiagnosticoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/diagnosticos")
public class DiagnosticoControlador {

    @Autowired
    private DiagnosticoServicio diagnosticoServicio;

    @GetMapping("/{id}")
    public Optional<Diagnostico> getDiagnostico(@PathVariable("id") Long id) {
        return diagnosticoServicio.getDiagnosticoById(id);
    }

    @PostMapping
    public Diagnostico crearDiagnostico(@RequestBody Diagnostico Diagnostico) {
        return diagnosticoServicio.crearDiagnostico(Diagnostico);
    }

    @PutMapping("/{id}")
    public Diagnostico guardarDiagnostico(@PathVariable("id") Long id, @RequestBody Diagnostico Diagnostico) {
        Diagnostico.setId(id);
        return diagnosticoServicio.guardarDiagnostico(Diagnostico);
    }

    @DeleteMapping("/{id}")
    public void eliminarDiagnostico(@PathVariable("id") Long id) {
        diagnosticoServicio.borrarDiagnostico(id);
    }

}