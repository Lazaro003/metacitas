package com.metaenlace.citas.controlador;

import com.metaenlace.citas.entidad.Paciente;
import com.metaenlace.citas.servicio.PacienteServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/pacientes")
public class PacienteControlador {

    @Autowired
    private PacienteServicio pacienteServicio;

    @GetMapping("/{id}")
    public Optional<Paciente> getPaciente(@PathVariable("id") Long id) {
        return pacienteServicio.getPacienteById(id);
    }

    @PostMapping
    public Paciente crearPaciente(@RequestBody Paciente paciente) {
        return pacienteServicio.crearPaciente(paciente);
    }

    @PutMapping("/{id}")
    public Paciente guardarPaciente(@PathVariable("id") Long id, @RequestBody Paciente paciente) {
        paciente.setId(id);
        return pacienteServicio.guardarPaciente(paciente);
    }

    @DeleteMapping("/{id}")
    public void borrarPaciente(@PathVariable("id") Long id) {
        pacienteServicio.borrarPaciente(id);
    }

}