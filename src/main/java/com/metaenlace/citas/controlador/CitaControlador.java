package com.metaenlace.citas.controlador;

import com.metaenlace.citas.entidad.Cita;
import com.metaenlace.citas.servicio.CitaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/citas")
public class CitaControlador {

    @Autowired
    private CitaServicio citaServicio;

    @GetMapping("/{id}")
    public Optional<Cita> getCita(@PathVariable("id") Long id) {
        return citaServicio.getCitaById(id);
    }

    @PostMapping
    public Cita crearCita(@RequestBody Cita cita) {
        return citaServicio.crearCita(cita);
    }

    @PutMapping("/{id}")
    public Cita guardarCita(@PathVariable("id") Long id, @RequestBody Cita cita) {
        cita.setId(id);
        return citaServicio.guardarCita(cita);
    }

    @DeleteMapping("/{id}")
    public void borrarCita(@PathVariable("id") Long id) {
        citaServicio.borrarCita(id);;
    }

}