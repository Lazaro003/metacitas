package com.metaenlace.citas.servicio;
import com.metaenlace.citas.entidad.*;
import com.metaenlace.citas.repositorio.DiagnosticoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DiagnosticoServicio {


    private DiagnosticoRepositorio diagnosticoRepositorio;

    public Optional<Diagnostico> getDiagnosticoById(Long id) {
        return diagnosticoRepositorio.findById(id);
    }
    public Diagnostico crearDiagnostico(Diagnostico diagnostico){
        return diagnosticoRepositorio.save(diagnostico);
    }
    public Diagnostico guardarDiagnostico(Diagnostico diagnostico){
        return diagnosticoRepositorio.save(diagnostico);
    }
    public void borrarDiagnostico(Long id){
        diagnosticoRepositorio.deleteById(id);;
    }
    public Optional<Diagnostico> buscarDiagnostico(Long id){
        return diagnosticoRepositorio.findById(id);}

    public List<Diagnostico> todosDiagnosticos(){
        return diagnosticoRepositorio.findAll();
    }

}
