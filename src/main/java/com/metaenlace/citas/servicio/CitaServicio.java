package com.metaenlace.citas.servicio;



import com.metaenlace.citas.entidad.Cita;
import com.metaenlace.citas.repositorio.CitaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CitaServicio {


    private CitaRepositorio citaRepositorio;

    public Optional<Cita> getCitaById(Long id) {
        return citaRepositorio.findById(id);
    }
    public Cita crearCita(Cita cita){

        return citaRepositorio.save(cita);
    }
    public Cita guardarCita(Cita cita){

        return citaRepositorio.save(cita);
    }
    public void borrarCita(Long id){
        citaRepositorio.deleteById(id);
    }
    public Optional<Cita> buscarCita(Long id){
        return citaRepositorio.findById(id);}

    public List<Cita> todosCitas(){
        return citaRepositorio.findAll();
    }

}