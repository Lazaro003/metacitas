package com.metaenlace.citas.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metaenlace.citas.entidad.*;
import com.metaenlace.citas.repositorio.*;


import java.util.List;
import java.util.Optional;
@Service
public class PacienteServicio {


    private PacienteRepositorio pacienteRepositorio;

    public Optional<Paciente> getPacienteById(Long id) {
        return pacienteRepositorio.findById(id);
    }

    public Paciente crearPaciente(Paciente paciente){

        return pacienteRepositorio.save(paciente);
    }
    public Paciente guardarPaciente(Paciente paciente){

        return pacienteRepositorio.save(paciente);
    }
    public void borrarPaciente(Long id){
        pacienteRepositorio.deleteById(id);;
    }
    public Optional<Paciente> buscarPaciente(Long id){
        return pacienteRepositorio.findById(id);}

    public List<Paciente> todosPacientes(){
        return pacienteRepositorio.findAll();
    }

}