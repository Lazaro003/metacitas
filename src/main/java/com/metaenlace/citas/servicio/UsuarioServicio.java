package com.metaenlace.citas.servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metaenlace.citas.entidad.*;
import com.metaenlace.citas.repositorio.*;


import java.util.List;
import java.util.Optional;
@Service
public class UsuarioServicio {


    private UsuarioRepositorio usuarioRepositorio;

    public Optional<Usuario> getUsuarioById(Long id) {
        return usuarioRepositorio.findById(id);
    }

    public Usuario crearUsuario(Usuario usuario){

        return usuarioRepositorio.save(usuario);
    }
    public Usuario guardarUsuario(Usuario usuario){

        return usuarioRepositorio.save(usuario);
    }
    public void borrarUsuario(Long id){
        usuarioRepositorio.deleteById(id);;
    }
    public Optional<Usuario> buscarUsuario(Long id){
        return usuarioRepositorio.findById(id);}

    public List<Usuario> todosUsuarios(){
        return usuarioRepositorio.findAll();
    }

}
