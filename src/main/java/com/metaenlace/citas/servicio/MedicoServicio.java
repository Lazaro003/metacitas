package com.metaenlace.citas.servicio;

import com.metaenlace.citas.entidad.*;
import com.metaenlace.citas.repositorio.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MedicoServicio {

    private MedicoRepositorio medicoRepositorio;

    public Optional<Medico> getMedicoById(Long id) {
        return medicoRepositorio.findById(id);
    }
    public Medico crearMedico(Medico medico){

        return medicoRepositorio.save(medico);
    }
    public Medico guardarMedico(Medico medico){

        return medicoRepositorio.save(medico);
    }
    public void borrarMedico(Long id){
        medicoRepositorio.deleteById(id);;
    }
    public Optional<Medico> buscarMedico(Long id){
        return medicoRepositorio.findById(id);}

    public List<Medico> todosMedicos(){
        return medicoRepositorio.findAll();
    }

}