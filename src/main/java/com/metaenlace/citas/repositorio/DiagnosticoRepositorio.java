package com.metaenlace.citas.repositorio;

import com.metaenlace.citas.entidad.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiagnosticoRepositorio extends JpaRepository<Diagnostico, Long> {
    List<Diagnostico> findByPaciente(Paciente paciente);
    List<Diagnostico> findByCita(Cita cita);
}