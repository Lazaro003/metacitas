package com.metaenlace.citas.repositorio;

import com.metaenlace.citas.entidad.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicoRepositorio extends JpaRepository<Medico, Long> {
    List<Medico> findByPaciente(Paciente paciente);
    List<Medico> findByCita(Cita cita);
}