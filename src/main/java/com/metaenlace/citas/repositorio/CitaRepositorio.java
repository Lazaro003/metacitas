package com.metaenlace.citas.repositorio;

import com.metaenlace.citas.entidad.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CitaRepositorio extends JpaRepository<Cita, Long> {
    List<Cita> findByFecha(LocalDateTime fecha);
    List<Cita> findByPaciente(Paciente paciente);
    List<Cita> findByMedico(Medico medico);
}
