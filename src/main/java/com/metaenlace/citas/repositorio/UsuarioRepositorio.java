package com.metaenlace.citas.repositorio;

import com.metaenlace.citas.entidad.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {
    List<Usuario> findByUsuario(Usuario usuario);
}
