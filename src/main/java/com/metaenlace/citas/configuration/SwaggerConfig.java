package com.metaenlace.citas.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI api(){
        return new OpenAPI().openapi("3.0.0").info(info());
    }

    public Info info(){
        return new Info().title("Citas Medicas").description("Api para citas medicas").version("0.0.1-SNAPSHOT");
    }
}
